var Game = function(id, players, gameOwner) {
	this._id = id;
	this.gameOwner = gameOwner;
	this.players = players;
	this.currentColor = null;
	this.deck = new Deck(generateCards());
	this.heap = new Heap();
	this.isTakiState = false;
	this.plus2counter = 0;
	this.statistics = {
		startTime: new Date(), turns: 0
	};
	this.renderer = new Renderer(this);
	this.winners = [];
	this.quiters = [];
	this.finished = false;
	this.steps = []; // for auditing and navigation between steps
	this.turnPosition = 0;
	this.navigationMode = false;
};

Game.prototype.removePlayer = function(game, player) {
	var index = game.players.players.indexOf(player);
	game.players.players.splice(index, 1);
};

Game.prototype.isCurrentHuman = function() {
	return this.players.getCurrentPlayer().isHuman;
};

Game.prototype.start = function() {
	var NUMBER_OF_CARDS_FOR_START = 8;
	this.finished = false;

	this.updateMessage('Starting !');
	this.deck.shuffle();
	this.deck.moveUntilValidTop(this.heap);
	var deck = this.deck;

	this.players.players.forEach(function(player) {
		player.cards = deck.pullCards(NUMBER_OF_CARDS_FOR_START);
	});

	addStep('start', null, this.players.getCurrentPlayer());

	this.renderer.render();

	if (!this.isCurrentHuman()) {
		game.turn();
	}
};

Game.prototype.getWinner = function() {
	var winner;

	if (!isEmpty(this.winners)) {
		winner = this.winners[0];
	}
	else {
		winner = this.players.get(0);
	}

	return winner;
};

Game.prototype.updateMessage = function(msg) {
	setHtml('message', 'Message: ' + msg);
};

Game.prototype.isGameOver = function() {
	return game.players.getCount() <= 1;
};

Game.prototype.finishTakiTurn = function() {
	this.turn(function(args) {
		this.isTakiState = false;
		this.heap.currentColor = null;
		hide('commit');
		return { updateTurnInfo: true, moveNextPlayer: true };
	});
};

Game.prototype.updateStatistics = function() {
	var stats = this.statistics;
	stats.turns++;
};

function onChangeColorClick(event) {
	var game = window.game;
	var args = {};
	args.event = event;
	var cardDiv = args.event.path[1];
	if (!cardDiv.id) {
		cardDiv = args.event.path[3];
	}

	var indices = getCardAndPlayerIndices(cardDiv.id);
	var cardIndex = indices[0];
	var playerIndex = indices[1];

	var player = game.players.get(playerIndex);
	var card = player.cards[cardIndex];

	if (card && card.operation === 'changeColor') {
		show(cardDiv.id + '-select-color');
	}
}

Game.prototype.turn = function(callback, args) {

	var result = {};

	if (callback) {
		result = callback(args);
	}

	if (result.updateTurnInfo) {
		this.updateStatistics();
	}
	if (result.moveNextPlayer) {
		this.updateMessage('Switch turn');
		this.players.moveNext();
	}
	if (result.step) {
		addStep(result.step.card, result.step.color, result.step.player);
	}

	var currentPlayer = this.players.getCurrentPlayer();

	if (isEmpty(currentPlayer.cards)) {
		this.removePlayer(this, currentPlayer);
		this.winners.push(currentPlayer);

	}
	else if (currentPlayer.cards.length === 1) {
		currentPlayer.singleCardCounter++;
	}

	if (this.players.players.length === 1) {
		var currentPlayer = this.players.getCurrentPlayer();
		this.removePlayer(this, currentPlayer);
		this.winners.push(currentPlayer);
	}

	if (this.isGameOver()) {
		if (this.winners.indexOf(currentPlayer) === -1) {
			this.winners.push(currentPlayer);
		}

		this.handleEndGame();
	}
	else {
		if (!this.isCurrentHuman()) {
			this.renderer.render();
			setTimeout(function() {
				raisePcEvent();
			}, 10);
		}
	}
	this.renderer.render();
};

Game.prototype.handleEndGame = function() {
	this.updateMessage('Game Finished!!!!!');

	var winner = game.getWinner();
	game.quiters.forEach(function(player) {
		game.winners.push(player);
	});
	hide('game');
	game.updateMessage('Game Finished!!!!! The Winner is ' + winner.name);
	document.getElementById('body-section').classList.add('finished');
};

function onSelectColor(color, event) {
	event.preventDefault();
	hide(event.currentTarget.parentElement.parentElement.id);

	var game = window.game;
	var args = {};
	args.event = event;

	game.turn(function(args) {
		var indices = getCardAndPlayerIndices(args.event.path[3].id);
		var cardIndex = indices[0];
		var playerIndex = indices[1];

		var player = game.players.get(playerIndex);
		var card = player.cards[cardIndex];

		player.putCard(game.heap, cardIndex);

		game.heap.currentColor = color.toUpperCase();

		var step = { card, color, player };
		return { updateTurnInfo: true, moveNextPlayer: true, step };
	}, args);
}

function addStep(card, color, player) {
	game.steps.push({
		card,
		color,
		player,
		players: JSON.parse(JSON.stringify(game.players)),
		deck: JSON.parse(JSON.stringify(game.deck)),
		heap: JSON.parse(JSON.stringify(game.heap)),
		winners: JSON.parse(JSON.stringify(game.winners)),
		quiters: JSON.parse(JSON.stringify(game.quiters))
	});
	game.turnPosition++;
	var action = card.operation || card.number;
	console.log(`card: ${action}, color: ${color}, player: ${player.name}`);
}

function onCardClicked(event) {
	var game = window.game;
	var args = {};
	args.event = event;
	game.turn(function(args) {
		var updateTurnInfo = true;
		var moveNextPlayer = true;
		var currentColor = game.heap.top().color || game.heap.currentColor;

		var indices = getCardAndPlayerIndices(args.event.path[1].id);
		var cardIndex = indices[0];
		var playerIndex = indices[1];

		var player = game.players.get(playerIndex);
		var card = player.cards[cardIndex];

		player.putCard(game.heap, cardIndex);

		if (card.operation === 'plus2') {
			game.plus2counter += 2;
			console.log(`Player: ${player.name} just put plus2. plus2counter: ${game.plus2counter}`);
		}
		else if (card.operation === 'superTaki') {
			game.heap.currentColor = currentColor;
			if (hasMoves(player, game)) {
				game.isTakiState = true;
				show('commit');
			}
		}
		else if (card.operation === 'taki') {
			if (hasMoves(player, game)) {
				game.isTakiState = true;
				game.heap.currentColor = card.color;
				show('commit');
			}
		}
		else if (card.operation === 'stop' && !game.isTakiState) {
			game.players.moveNext();
		}
		else if (card.operation === 'plus') {
			moveNextPlayer = false;
		}
		if (game.isTakiState) {
			updateTurnInfo = false;
			moveNextPlayer = false;
		}
		var step = { card, color: card.color, player };
		return { updateTurnInfo, moveNextPlayer, step };
	}, args);
}

function onCommitTurn() {
	var game = window.game;
	game.finishTakiTurn();
}

function onNextClick() {
	var game = window.game;
	game.turnPosition++;
	var s = game.steps[game.turnPosition];
	loadGameByState(s);
	window.game.renderer.render();
}

function onPrevClick() {
	var game = window.game;
	game.turnPosition--;
	var s = game.steps[game.turnPosition];
	loadGameByState(s);
	window.game.renderer.render();
}

function loadGameByState(state) {
	var game = window.game;
	if (state) {
		cloneObject(state.players, game.players);
		cloneObject(state.deck, game.deck);
		cloneObject(state.heap, game.heap);
		cloneObject(state.winners, game.winners);
		cloneObject(state.quiters, game.quiters);
	}
}

function cloneObject(src, target) {
	for (var i in src) {
		if (target) {
			if (src[i] != null && typeof(src[i]) == 'object' && !Array.isArray(src[i])) {
				cloneObject(src[i], target[i]);
			}
			else {
				target[i] = src[i];
			}
		}
	}
}

function onDeckClicked() {
	var game = window.game;
	var currentPlayer = game.players.getCurrentPlayer();
	if (hasMoves(currentPlayer, game)) {
		game.updateMessage('Card cannot be pulled when there are valid cards to put');
		return;
	}

	game.turn(function() {
		currentPlayer.pullCard();
		hide('commit');
		var step = { card: { operation: 'pull' }, color: null, player: currentPlayer };
		return { updateTurnInfo: true, moveNextPlayer: true, step };
	});
}

function onQuite() {
	var game = window.game;
	var player = game.players.getCurrentPlayer();
	game.quiters.push(player);
	game.removePlayer(game, player);
	game.turn();
}

function onRestart() {
	hide('next');
	hide('prev');
	hide('restartGame');
	show('quite');
	startGame();

}

// PC Custom event
function handlePcEvent(event) {
	var args = event.detail;
	var game = window.game;

	game.turn(function(args) {
		var currentPlayer = game.players.getCurrentPlayer();
		var move = currentPlayer.getPcMove(game);
		var tmp = (!isEmpty(move.cards)) ? move.cards[0].card.stringify() : '<>'; // TODO: remove
		var updateTurnInfo = true;
		var moveNextPlayer = true;
		var card = (move.cards) ? move.cards[0].card : { operation: 'pull' };

		if (move.action === 'pull') {
			currentPlayer.pullCard();
			game.isTakiState = false;
		}
		else if (move.action === 'plus2') {
			game.plus2counter += 2;
			console.log(`Player: ${currentPlayer.name} just put plus2. plus2counter: ${game.plus2counter}`);
		}
		else if (move.action === 'changeColor') {
			game.heap.setRandomColor();

		}
		else if (move.action === 'superTaki') {
			game.heap.currentColor = game.heap.top().color;
			game.isTakiState = true;
			moveNextPlayer = false;
			updateTurnInfo = false;

		}
		else if (move.action === 'taki') {
			moveNextPlayer = false;
			updateTurnInfo = false;
		}
		else if (move.action === 'stop') {
			game.players.moveNext();
		}
		else if (move.action === 'plus') {
			moveNextPlayer = false;
		}

		if (move.cards) {
			move.cards.forEach(function(c) {
				currentPlayer.putCard(game.heap, c.index);
			});
		}
		var step = { card, color: card.color, player: currentPlayer };
		return { updateTurnInfo, moveNextPlayer, step };
	});
}

function raisePcEvent() {
	var event = new CustomEvent('pc-event', {
		detail: {}
	});
	window.dispatchEvent(event);
}

function startGame() {
	var url = window.location.href;
	var numHumanPlayers = gup('num_human_players', url) || 1;
	var numPcPlayers = gup('num_pc_players', url) || 1;

	// hide('commit');

	var participants = new Participants();
	participants.generate(numHumanPlayers, numPcPlayers);

	var game = new Game('<game-id>', participants);
	window.game = game;
	window.game.finished = false;
	showGameStatisitics();
	// TODO: move to game component
	document.getElementById('body-section').classList.remove('finished');
	show('game');
	game.start();
}

function showGameStatisitics() {
	show('current-player');
	show('turn-counter');
	show('color');
	show('duration');
	show('single-card-counter');
	show('avg-turn-duration');
	show('message');
}

function onNavigate() {
	var game = window.game;
	game.navigationMode = true;
	show('next');
	show('prev');
	show('restartGame');
	hide('commit');
	hide('quite');
	document.getElementById('body-section').classList.remove('finished');
	show('game');
	var s = game.steps[0];
	loadGameByState(s);
	game.turnPosition = 0;
	game.renderer.render();
}

window.onload = function(e) {
	window.addEventListener('pc-event', handlePcEvent, false);

	startGame();

	setInterval(function() {
		game.renderer.gameInfo.render();
	}, 100);
};
