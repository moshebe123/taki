import React from 'react';
import ReactDOM from 'react-dom';

export default class PlayerInfo extends React.Component {
	constructor(args) {
		super(...args);
		this.state = {
			disabled: args.disabled, player: args.player
		};
	}

	render() {
		const { player, disabled } = this.state;
		const infoText = `player name: ${player.name} | number of cards: ${player.cards.length}`;
		return (<div className="player-info">
			<span>{infoText}</span>
		</div>);
	}
}