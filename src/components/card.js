import React from 'react';
import Validations from '../helpers/Validations';
import ColorPickerComponent from './colorPicker';
import ViewUtils from '../helpers/ViewUtils';

export default class Card extends React.Component {
	constructor(args) {
		super(...args);

		this.state = {
			selectColor: false
		};
		this.onClick = this.onClick.bind(this);
		this.handleColorChoosed = this.handleColorChoosed.bind(this);
	}

	onClick() {
		if (this.props.game.players.players[this.props.game.players.current].name !== localStorage.getItem('myusername')) {
			return;
		}
		if (this.props.card.operation === 'changeColor') {
			this.state.selectColor = true;
		}
		else {
			this.state.selectColor = false;
			this.props.cardClicked(this.props);

		}
		this.setState(this.state);
	}

	handleColorChoosed(color) {
		this.state.selectColor = false;
		this.setState(this.state);
		const { card, player } = this.props;
		this.props.onColorChoosed(color, card, player);
	}

	render() {
		const { card, player, game, onColorChoosed } = this.props;

		let clickHandler = null;
		let filepath;
		let className = card.color;

		const myUsername = localStorage.getItem('myusername');
		const isMe = myUsername === player.name;

		if (isMe) {
			filepath = new ViewUtils().getFilepath(null, card);

			if (new Validations().isValidCard(card, game) && !game.navigationMode) {
				clickHandler = this.onClick;
			}
			else {
				className += ' disabled';
			}
		}
		else {
			filepath = new ViewUtils().getFilepath('cover.png');
		}

		if (this.state.selectColor) {
			return (<div className="card-container">
				<img src={filepath} className={className} onClick={clickHandler}/>
				<ColorPickerComponent player={player} card={card} onColorChoosed={(color) => this.handleColorChoosed(color)}/>
			</div>);
		}
		else {
			return (<div className="card-container" onClick={clickHandler}>
				<button>
					<img src={filepath} className={className}/>
				</button>
			</div>);
		}
	}
}