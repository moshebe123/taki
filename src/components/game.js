import React from 'react';
import BoardComponent from './board';
import StatusBarComponent from './statusBar';
import StatisticsComponent from './statistics';

import Validations from '../helpers/Validations';
import GameCreator from '../helpers/gameCreator';
import Game from '../models/Game';
import Player from './player';

export default class Game extends React.Component {
	constructor(args) {
		super(...args);
		this.state = {
			game: args.game, heapClass: ''

		};

		this.cardClicked = this.cardClicked.bind(this);
		this.deckClicked = this.deckClicked.bind(this);
		this.commitTaki = this.commitTaki.bind(this);
		this.quitClicked = this.quitClicked.bind(this);
		this.onColorChoosed = this.onColorChoosed.bind(this);
		this.onRestart = this.onRestart.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.handlePcEvent = this.handlePcEvent.bind(this);
		this.onInit = this.onInit.bind(this);
	}

	componentDidMount() {
		window.addEventListener('pc-event', this.handlePcEvent, false);
	}

	componentWillUnmount() {
		window.removeEventListener('pc-event', this.handlePcEvent, false);
	}

	handlePcEvent(event) {
		console.log('handlePcEvent called:', this.state, this.props);
		event.stopPropagation();

		const { game } = this.state;
		var that = this;

		game.turn(function(args) {
			console.log('called inner pc');
			const game = args.game;

			var currentPlayer = new Participants().getCurrentPlayer(game.players);
			if (!currentPlayer) {
				//TODO
			}
			var move = currentPlayer.getPcMove(game);
			var card = (move.cards) ? move.cards[0] : { operation: 'pull', color: undefined };

			var updateTurnInfo = true;
			var moveNextPlayer = true;

			if (move.action === 'pull') {
				new Player().pullCard(currentPlayer, game);
				game.isTakiState = false;
			}
			else if (move.action === 'plus2') {
				game.isTakiState = false;
				game.plus2counter += 2;
				console.log(`Player: ${currentPlayer.name} just put plus2. plus2counter: ${game.plus2counter}`);
			}
			else if (move.action === 'changeColor') {
				game.isTakiState = false;
				game.heap.setRandomColor();

			}
			else if (move.action === 'superTaki') {
				game.isTakiState = true;
				game.heap.currentColor = new Heap().top(game.heap).color;
				moveNextPlayer = false;
				updateTurnInfo = false;

			}
			else if (move.action === 'taki') {
				moveNextPlayer = false;
				updateTurnInfo = false;

			}
			else if (move.action === 'stop') {
				game.isTakiState = false;
				new Participants().moveNext(game.players);

			}
			else if (move.action === 'plus') {
				game.isTakiState = false;
				moveNextPlayer = false;
			}

			if (move.cards) {
				move.cards.forEach(function(c) {
					currentPlayer.putCard(game.heap, c);
					that.setHeapAnimation();
				});
			}
			var step = { card, color: card.color, player: currentPlayer };
			return { updateTurnInfo, moveNextPlayer, step };
		}, { game });

		this.setState(this.state);
	}

	deckClicked() {
		console.log('Game deckClicked');
		const { game } = this.state;
		this.setState(this.state);

		var currentPlayer = new Participants().getCurrentPlayer(game.players);

		if (Validations.hasMoves(currentPlayer, game)) {
			game.updateMessage('Card cannot be pulled when there are valid cards to put');
			return;
		}

		game.turn(function(args) {
			new Player().pullCard(currentPlayer, args.game);
			return { updateTurnInfo: true, moveNextPlayer: true };
		}, { game });

		this.setState(this.state);

	}

	cardClicked(cardState) {
		const { player, card } = cardState;

		const game = this.state.game;
		var heapClass = this.state.heapClass;

		var args = {};
		args.card = card;
		args.player = player;
		args.game = game;

		game.turn(function(args) {
			var updateTurnInfo = true;
			var moveNextPlayer = true;

			var player = args.player;
			var card = args.card;
			var game = args.game;

			var currentColor = new Heap().top(game.heap).color || game.heap.currentColor;

			player.putCard(game.heap, card);
			heapClass = 'start';
			if (card.operation === 'plus2') {
				game.plus2counter += 2;
				console.log(`Player: ${player.name} just put plus2. plus2counter: ${game.plus2counter}`);
			}
			else if (card.operation === 'superTaki') {
				game.heap.currentColor = currentColor;
				if (Validations.hasMoves(player, game)) {
					game.isTakiState = true;
				}
			}
			if (card.operation === 'taki') {
				if (Validations.hasMoves(player, game)) {
					game.isTakiState = true;
					game.heap.currentColor = card.color;
				}
			}
			else if (card.operation === 'stop' && !game.isTakiState) {
				new Participants().moveNext(game.players);
			}
			else if (card.operation === 'plus') {
				moveNextPlayer = false;
			}
			//hide('commit');
			if (game.isTakiState) {
				updateTurnInfo = false;
				moveNextPlayer = false;
			}
			var step = { card, color: card.color, player };
			return { updateTurnInfo, moveNextPlayer, step };
		}, args);
		this.setHeapAnimation();
		this.setState(this.state);
	}

	setHeapAnimation() {
		this.state.heapClass = 'start';
		this.setState(this.state);
		var that = this;
		setTimeout(function() {
			that.state.heapClass = '';
			that.setState(that.state);
		}, 1500);
	}

	commitTaki() {
		console.log('Game commitTaki');

		new Game().finishTakiTurn(this.state.game);

		this.setState(this.state);
	}

	quitClicked() {
		console.log('Game quitClicked');

		const { game } = this.state;

		const player = new Participants().getCurrentPlayer(game.players);
		game.quiters.push(player);
		new Game().removePlayer(game, player);
		game.turn();
		this.setState(this.state);
	}

	onColorChoosed(color, card, player) {
		const { game } = this.state;
		var that = this;
		game.turn(function(args) {
			const { game, card, player } = args;

			player.putCard(game.heap, card);
			game.heap.currentColor = color.toUpperCase();

			var step = { card, color, player };
			return { updateTurnInfo: true, moveNextPlayer: true, step };
		}, { game, color, card, player });
	}

	onRestart() {
		fetch(`/games/restart/${this.state.game._id}`, { method: 'POST', body: {}, credentials: 'include' })
			.then(response => {
				if (!response.ok) {
					return false;
				}
				return response;
			});
		this.setState(this.state);
	}

	loadGameByState(state) {
		var game = this.state.game;
		if (state) {
			this.cloneObject(state.players, game.players);
			this.cloneObject(state.deck, game.deck);
			this.cloneObject(state.heap, game.heap);
			this.cloneObject(state.winners, game.winners);
			this.cloneObject(state.quiters, game.quiters);
		}
	}

	cloneObject(src, target) {
		for (var i in src) {
			if (target) {
				if (src[i] != null && typeof(src[i]) == 'object' && !Array.isArray(src[i])) {
					this.cloneObject(src[i], target[i]);
				}
				else {
					target[i] = src[i];
				}
			}
		}
	}

	render() {
		const { game, heapClass } = this.state;

		if ((new Game().isGameOver(game) || game.finished)) {
			return (<StatisticsComponent game={game} onRestart={this.onRestart} onInit={this.onInit}/>);
		}
		else {
			return (<div>
				<BoardComponent game={game} quitClicked={this.quitClicked}
												cardClicked={this.cardClicked} onColorChoosed={this.onColorChoosed}
												deckClicked={this.deckClicked} commitTaki={this.commitTaki}
												onRestart={this.onRestart}
												heapClass={heapClass}
				/>
				<StatusBarComponent game={game}/>
			</div>);
		}
	}
}