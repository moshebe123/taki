import React from 'react';

const Utils = require('../helpers/Utils');
const Game = require('../models/Game');

export default class Statistics extends React.Component {
	constructor(args) {
		super(...args);
		this.state = {
			game: null
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData() {
		const gameTitle = this.props.game._id;

		fetch('/games/state/' + gameTitle, { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {
			this.setState({ game: content });
		});
	}

	render() {

		const game = this.state.game;
		if (!game) {
			return 'Wait for the results...';
		}

		const { onRestart, onNavigate, onInit } = this.props;
		const { finished, statistics, winners } = game.game;
		const duration = new Utils().millisToMinutesAndSeconds(
			(new Date(statistics.endGame) - new Date(statistics.startTime)));
		const winner = new Game().getWinner(game.game);
		const turns = statistics.turns;

		return (<div className="statistic-body">
			<div className="statistic-section">
				<span className="title">Statistics</span>
				<div>
					<span>The winner is: {winner && winner.name}</span>
					<span>Number of turns: {turns}</span>
					<span>Game duration: {duration}</span>
					<table>
						<thead>
							<tr>
								<th>player name</th>
								<th>average turn duration</th>
								<th>Number of single card</th>
							</tr>
						</thead>
						<tbody id="player-statistics">
							{winners.map((w, i) => <tr key={i}>
								<td>{w.name}</td>
								<td>{new Utils().millisToMinutesAndSeconds(w.statistics.averageTurnDuration)}</td>
								<td>{w.singleCardCounter}</td>
							</tr>)}
						</tbody>
					</table>
				</div>
				<div className="end-button-container">
					<button id="restart" onClick={onInit}>Init Game</button>
				</div>
			</div>
		</div>);
	}
}