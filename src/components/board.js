import React from 'react';
import DecksComponent from './decks';
import PlayerComponent from './player';
import Participants from '../models/Participants';

export default class Board extends React.Component {

	render() {
		const { gameStarted, game, cardClicked, onColorChoosed, deckClicked, commitTaki, quitClicked, nextClick, prevClick, onRestart, heapClass } = this.props;

		if (!gameStarted){
			return (<div>Loading...</div>);
		}
		
		const currentPlayer = new Participants().getCurrentPlayer(game.players);
		const myUsername = localStorage.getItem('myusername');
		const isMe = myUsername === currentPlayer.name;

		const isNextDisable = (game.turnPosition >= 0 && game.turnPosition < game.steps.length) ? false : true;
		const isPrevDisable = (game.turnPosition >= 1) ? false : true;

		return (<div id="game" className="game-flex">
				<DecksComponent game={game} deckClicked={deckClicked} heapClass={heapClass}/>
				<div className="end-button-container">
					{game.isTakiState && isMe && <button id="commit" onClick={commitTaki}>Closed Taki</button>}
					{<button id="quite" onClick={quitClicked}>Quit</button>}
					{!game.finished && <button id="restartGame" className="action-button" onClick={onRestart}>Restart</button>}
				</div>
				<div id="players">
					{game.players.players.map(
						(player, i) => <PlayerComponent key={i} player={player} game={game} cardClicked={cardClicked}
																						onColorChoosed={onColorChoosed}/>)}
				</div>
			</div>

		);
	}
}