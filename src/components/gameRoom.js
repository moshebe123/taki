import React from 'react';

import BoardComponent from './board';
import StatusBarComponent from './statusBar';
import StatisticsComponent from './statistics';

import GameCreator from '../helpers/gameCreator';
import Game from '../models/Game';
import Player from './player';
import Logout from './logout';

export default class GameRoom extends React.Component {
	constructor(args) {
		super(...args);
		this.state = {
			game: null, heapClass: '', dataLoaded: false
		};

		this.cardClicked = this.cardClicked.bind(this);
		this.deckClicked = this.deckClicked.bind(this);
		this.commitTaki = this.commitTaki.bind(this);
		this.quitClicked = this.quitClicked.bind(this);
		this.onColorChoosed = this.onColorChoosed.bind(this);
		this.onRestart = this.onRestart.bind(this);
		this.onInit = this.onInit.bind(this);
		this.handlePcEvent = this.handlePcEvent.bind(this);
	}

	componentDidMount() {
		window.addEventListener('pc-event', this.handlePcEvent, false);
		this.getData();
		this.interval = setInterval(() => this.getData(), 1000);
	}

	componentWillUnmount() {
		window.removeEventListener('pc-event', this.handlePcEvent, false);
		clearInterval(this.interval);
	}

	handlePcEvent(event) {
		console.log('handlePcEvent called:', this.state, this.props);
		event.stopPropagation();

		const { game } = this.state;
		var that = this;

		game.turn(function(args) {
			console.log('called inner pc');
			const game = args.game;

			var currentPlayer = new Participants().getCurrentPlayer(game.players);
			if (!currentPlayer) {
				//TODO
			}
			var move = currentPlayer.getPcMove(game);
			var card = (move.cards) ? move.cards[0] : { operation: 'pull', color: undefined };

			var updateTurnInfo = true;
			var moveNextPlayer = true;

			if (move.action === 'pull') {
				new Player().pullCard(currentPlayer, game);
				game.isTakiState = false;
			}
			else if (move.action === 'plus2') {
				game.isTakiState = false;
				game.plus2counter += 2;
				// console.log(`Player: ${currentPlayer.name} just put plus2. plus2counter: ${game.plus2counter}`);
			}
			else if (move.action === 'changeColor') {
				game.isTakiState = false;
				game.heap.setRandomColor();

			}
			else if (move.action === 'superTaki') {
				game.isTakiState = true;
				game.heap.currentColor = new Heap().top(game.heap).color;
				moveNextPlayer = false;
				updateTurnInfo = false;

			}
			else if (move.action === 'taki') {
				moveNextPlayer = false;
				updateTurnInfo = false;

			}
			else if (move.action === 'stop') {
				game.isTakiState = false;
				new Participants().moveNext(game.players);

			}
			else if (move.action === 'plus') {
				game.isTakiState = false;
				moveNextPlayer = false;
			}

			if (move.cards) {
				move.cards.forEach(function(c) {
					currentPlayer.putCard(game.heap, c);
					that.setHeapAnimation();
				});
			}
			var step = { card, color: card.color, player: currentPlayer };
			return { updateTurnInfo, moveNextPlayer, step };
		}, { game });
	}

	deckClicked() {
		const payload = {
			game: this.state.game.title
		};

		fetch('/games/pullDeck', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' })
			.then(response => {
				this.getData();
			});

		this.setHeapAnimation();
		this.setState(this.state);
	}

	cardClicked(cardState) {
		const { player, card } = cardState;
		const game = this.state.game;
		var heapClass = this.state.heapClass;

		var args = {};
		args.card = card;
		args.player = player;
		args.game = game;

		const payload = {
			game: game.title, card: card
		};

		fetch('/games/putCard', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' })
			.then(response => {
				this.getData();
			});

		this.setHeapAnimation();
	}

	setHeapAnimation() {
		this.state.heapClass = 'start';
		this.setState(this.state);
		var that = this;
		setTimeout(function() {
			that.state.heapClass = '';
			that.setState(that.state);
		}, 1500);
	}

	commitTaki() {
		const payload = {
			game: this.state.game.title
		};
		fetch('/games/commitTaki', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' })
			.then(response => {
				this.getData();
			});
	}

	quitClicked() {
		const payload = {
			game: this.state.game.title
		};

		fetch('/games/quit', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' }).then(response => {
			this.getData();
		});
	}

	onColorChoosed(color, card, player) {
		const payload = {
			game: this.state.game.title, color: color, card: card
		};

		fetch('/games/colorChoose', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' })
			.then(response => {
				this.getData();
			});
	}

	onRestart() {
		fetch(`/games/restart/${this.state.game.game._id}`, { method: 'GET', body: {}, credentials: 'include' })
			.then(response => {
				if (!response.ok) {
					return false;
				}
				return response;
			});
		this.setState(this.state);
	}

	onInit() {
		const { game } = this.state;

		const payload = {
			gameId: game.game._id
		};

		fetch('/games/init', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			this.props.changeState({ joinedGame: null });
			return response;
		});
	}

	loadGameByState(state) {
		var game = this.state.game;
		if (state) {
			this.cloneObject(state.players, game.players);
			this.cloneObject(state.deck, game.deck);
			this.cloneObject(state.heap, game.heap);
			this.cloneObject(state.winners, game.winners);
			this.cloneObject(state.quiters, game.quiters);
		}
	}

	cloneObject(src, target) {
		for (var i in src) {
			if (target) {
				if (src[i] != null && typeof(src[i]) == 'object' && !Array.isArray(src[i])) {
					this.cloneObject(src[i], target[i]);
				}
				else {
					target[i] = src[i];
				}
			}
		}
	}

	getData() {
		const title = this.props.title;
		fetch('/games/state/' + title, { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(game => {
			if (game) {
				this.setState({ game, dataLoaded: true });
				if (game.numRegistered === 0) {
					this.props.changeState({ joinedGame: null });
				}
			}
			else {
				console.error('Error while fetching data: ', game);
			}
		});
	}

	handleLoggedOut() {
		this.props.onLoggedOut();
	}

	render() {
		const { game, heapClass, dataLoaded } = this.state;

		if (!dataLoaded) {
			return (<div><Logout onLoggedOut={this.handleLoggedOut.bind(this)}/>
				<div className="info-msg board-page">Loading...</div>
			</div>);
		}

		if (!game.started) {
			return (<div><Logout onLoggedOut={this.handleLoggedOut.bind(this)}/>
				<div className="info-msg board-page">Waiting for participants...</div>
			</div>);
		}

		if ((new Game().isGameOver(game.game) || game.game.finished)) {
			return (<div><Logout onLoggedOut={this.handleLoggedOut.bind(this)}/>
				<StatisticsComponent game={game.game} onRestart={this.onRestart} onInit={this.onInit}/>
			</div>);
		}

		return (<div>
			<BoardComponent gameStarted={game.started} game={game.game} quitClicked={this.quitClicked}
											cardClicked={this.cardClicked} onColorChoosed={this.onColorChoosed}
											deckClicked={this.deckClicked} commitTaki={this.commitTaki}
											onRestart={this.onRestart}
											heapClass={heapClass}
			/>
			<StatusBarComponent game={game.game}/>
		</div>);

	}
}