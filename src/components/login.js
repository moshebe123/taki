import React from 'react';

export default class Login extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			errorMessage: ''
		};
	}

	componentWillMount() {
		fetch('/users/', { method: 'GET', credentials: 'include' }).then(response => {
			return response.ok;
		}).then(logged => {
			const onLogin = this.props.onLogin;
			onLogin && onLogin(logged);
		});
	}

	handleLogin(e) {
		e.preventDefault();

		let errorMessage;
		const { onLogin } = this.props;
		const userName = e.target.elements.userName.value;

		if (userName) {
			fetch('/users/addUser', { method: 'POST', body: userName, credentials: 'include' }).then(response => {
				const isLogged = response.ok;

				if (isLogged) {
					errorMessage = '';
				}
				else {
					errorMessage = 'user already exists';
				}

				localStorage.setItem('myusername', userName);
				this.setState({ errorMessage });

				onLogin && onLogin(isLogged);
			});
		}
		return false;
	}

	render() {

		return (<div className="login">
			<div>Sign in</div>

			<form onSubmit={this.handleLogin.bind(this)}>
				<input type="text" name="userName"/>
				<input className="submit-btn btn" type="submit" value="Login"/>
			</form>
			<p className="info-msg">{this.state.errorMessage}</p>
		</div>);
	}
}