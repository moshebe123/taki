import React from 'react';
import ReactDOM from 'react-dom';

export default class colorPicker extends React.Component {
	render() {
		return (<div className="select-color">
			<div><span>Select Color</span></div>
			<div className="colors">
				<button className="green" id="color-selector-green" onClick={() => this.props.onColorChoosed('green')}/>
				<button className="blue" id="color-selector-blue" onClick={() => this.props.onColorChoosed('blue')}/>
				<button className="red" id="color-selector-red" onClick={() => this.props.onColorChoosed('red')}/>
				<button className="yellow" id="color-selector-yellow" onClick={() =>this.props.onColorChoosed('yellow')}/>
			</div>
		</div>);
	}
}