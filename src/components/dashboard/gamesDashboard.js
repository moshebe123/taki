import React from 'react';
import UsersListComponent from './usersList';
import GamesListComponent from './gamesList';
import GameCreation from './gameCreation';

export default class GamesDashboard extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.getGames();
		this.interval = setInterval(() => this.getGames(), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	getGames() {
		fetch('/games/allGames', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {
			this.setState({ gamesList: content });
		});
	}

	createGame(e) {
		e.preventDefault();

		const values = e.target.elements;
		const payload = {
			title: values.gameTitle.value, owner: values.gameOwner.value, numParticipants: values.gameNumParticipants.value
		};

		fetch('/games/create', { method: 'POST', body: payload, credentials: 'include' }).then(response => {
			console.log('successful game creation');
		});

		return false;
	}

	printGames(gamesList) {
		return (gamesList.map((game, i) => (<tr key={i}>
			<td>{game.title}</td>
			<td>{game.owner}</td>
			<td>{game.numParticipants}</td>
			<td>{game.numRegistered}</td>
		</tr>)));
	}

	render() {

		return (<div>
			<GameCreation/>
			<div style={{ display: 'flex', flexDirection: 'row' }} className="dashboard">
				<UsersListComponent/>
				<GamesListComponent onJoined={this.props.onJoined}/>

			</div>
		</div>);
	}
}