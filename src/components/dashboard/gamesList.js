import React from 'react';
import GameRowComponent from './gameRow';

export default class GamesListComponent extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			gamesList: []
		};
	}

	componentDidMount() {
		this.getGames();
		this.interval = // TODO: stop interval in case that stop displaying (move to the game screen or logout)
			this.interval = setInterval(() => this.getGames(), 1000 * 2);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	getGames() {
		fetch('/games/allGames', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {
			this.setState({ gamesList: content });
		});
	}

	printGames(gamesList) {
		return (gamesList && gamesList.map(
				(g, i) => (<GameRowComponent key={i} game={g.game} onJoined={this.props.onJoined}/>)));
	}

	render() {
		const { gamesList = [] } = this.state;

		return (<div style={{ flex: 3 }} className="login game-table games">
				<div>Games</div>
				<div className="table-container">
					<table>
						<thead>
							<tr>
								<th>Title</th>
								<th>Owner</th>
								<th>Participants</th>
								<th>Registered</th>
								<th>Started</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{this.printGames(gamesList)}
						</tbody>
					</table>
				</div>
			</div>);
	}
}