import React from 'react';

export default class UsersListComponent extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			userList: []
		};
	}

	componentDidMount() {
		this.getUsers();
		this.interval = setInterval(() => this.getUsers(), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	getUsers() {
		fetch('/users/allUsers', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {
			this.setState({ userList: content });
		});
	}

	printUsers(userList) {
		return (userList.map((user, i) => (<tr key={i}>
					<td>{user.name}</td>
				</tr>)));
	}

	render() {
		const { userList = [] } = this.state;

		return (<div style={{ flex: 1 }} className="login game-table users">
				<div>Users</div>
				<div className="table-container">
					<table>
						<thead>
							<tr>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							{this.printUsers(userList)}
						</tbody>
					</table>
				</div>
			</div>);
	}
}