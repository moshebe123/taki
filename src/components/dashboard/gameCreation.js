import React from 'react';

export default class GameCreation extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			errorMessage: ''
		};
	}

	setError(msg) {
		this.setState({ errorMessage: msg });
	}

	createGame(e) {
		e.preventDefault();

		const values = e.target.elements;
		const payload = {
			title: values.gameTitle.value, numParticipants: values.gameNumParticipants.value
		};

		if (values.gameNumParticipants.value < 2 || values.gameNumParticipants.value > 4) {
			this.setState({ errorMessage: 'invalid number of participants' });
			return;
		}

		fetch('/games/create', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' })
			.then(response => {
				if (response.ok) {
					document.getElementById('add-game').reset();
				}
				else {
					return response.text();
				}
			}).then(msg => {
			this.setState({ errorMessage: msg });
		});

		return false;
	}

	render() {

		return (<div className="add-game">
			<form onSubmit={this.createGame.bind(this)} id="add-game">
				{/*<label className="gameTitle-label" htmlFor="gameTitle">Title: </label>*/}
				<input className="gameTitle-input" name="gameTitle" placeholder="Game Title Here..."/>

				{/*<label className="gameNumParticipants-label" htmlFor="gameNumParticipants">Num Participant: </label>*/}
				<input className="gameNumParticipants-input" name="gameNumParticipants"
							 placeholder="Number of Participants Here..."/>

				<input className="create-btn submit-btn btn" type="submit" value="Add Game"/>
			</form>
			<span className="info-msg">{this.state.errorMessage}</span>
		</div>);
	}
}