import React from 'react';

export default class GameRowComponent extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			gamesList: null, gameTitle: null, currentUser: localStorage.getItem('myusername')
		};
	}

	componentDidMount() {
		this.getGames();
		this.interval = setInterval(() => this.getGames(), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	setGamesList(gamesList) {
		this.state.gamesList = gamesList;
		this.setState(this.state);
	}

	getGames() {
		fetch('/games/allGames', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			const gamesList = response.json();
			this.setGamesList(gamesList);
			return gamesList;
		}).then(content => {
		});
	}

	joinGame(e) {
		const game = this.props.game;
		console.log('join game: ', game);

		this.state.gameTitle = game.title;
		this.setState(this.state);

		const payload = {
			title: game.title, name: localStorage.getItem('myusername')
		};

		fetch('/games/join', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response;
		}).then(content => {
			const { onJoined } = this.props;
			onJoined && onJoined(this.state.gameTitle);
			// TODO: move to next screen
			// console.log('JOINED !!', content);
			// this.setState({ gamesList: content })
		});
	}

	removeGame(e) {
		const game = this.props.game;

		this.state.gameTitle = game.title;
		this.setState(this.state);

		const payload = {
			gameId: game.game._id
		};

		fetch('/games/remove', { method: 'POST', body: JSON.stringify(payload), credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response;
		});
	}

	render() {
		const { game, c } = this.props;

		return (<tr>
			<td>{game.title}</td>
			<td>{game.owner}</td>
			<td>{game.numParticipants}</td>
			<td>{game.numRegistered}</td>
			<td>{game.started ? 'Yes' : 'No'}</td>
			<td> {!game.started ? <a href="#" onClick={e => this.joinGame(
				e)}>Join</a> : ''} {!game.started && game.numRegistered == 0 && game.owner === this.state.currentUser ? <a
				href="#" onClick={e => this.removeGame(e)}>Remove</a> : ''}
			</td>
		</tr>);
	}
}