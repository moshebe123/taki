import React from 'react'

export default class GamesDashboard extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			userList: []
		}
	}
	
	componentDidMount() {
		console.log('Users mount');
		this.getUsers();

		this.usersInterval = setInterval(() => this.getUsers(), 1000);
		this.gamesInterval = setInterval(() => this.getGames(), 1000);
	}
	
	componentWillUnmount() {
		clearInterval(this.usersInterval);
		clearInterval(this.gamesInterval);
	}
	
	getUsers() {
		fetch('/users/allUsers', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {	
			this.setState({ userList: content })
		})
	}
	
	getGames() {
		fetch('/games/allGames', { method: 'GET', credentials: 'include' }).then(response => {		
			if (!response.ok) {
				return false;
			}
			return response.json();
		}).then(content => {
			this.setState({ gamesList: content })
		})
	}
	
	createGame(e){
		e.preventDefault();
		
		const gameTitle = e.target.elements.gameTitle.value;
		const gameOwner = e.target.elements.gameOwner.value;
		const gameNumParticipants = e.target.elements.gameNumParticipants.value;		

		fetch('/games', { method: 'POST', body: userName, credentials: 'include' }).then(response => {
			onLogin && onLogin(response);
		});

		return false;
	}

	printGames(gamesList) {
		return (
			gamesList.map((game,i) => (
				<tr key={i}>
				<td>{game.title}</td>
				<td>{game.owner}</td>
				</tr>
			))
		)
	}
	
	printUsers(userList) {
		return (
			userList.map((user,i) => (
				<tr key={i}>
				<td>{user.name}</td>
				<td>{user.id}</td>
				</tr>
			))
		)
	}


	
	render() {
		const { userList = [], gamesList = [] } = this.state;
		
		return (
			<div style={{display:'flex', flexDirection:'row'}}>			
				<div style={{flex:1}}>
					<h2>Users</h2>
					
					<table>
					<thead>
					<tr>
					<th>Name</th>
					<th>Join</th>
					</tr>
					</thead>
					<tbody>
					{this.printUsers(userList)}
					</tbody>
					</table>
				</div>
				<div style={{flex:1}}>				
					<h2>Games</h2>
					<table>
					<thead>
					<tr>
					<th>Title</th>
					<th>Owner</th>
					</tr>
					</thead>
					<tbody>
					{this.printGames(gamesList)}
					</tbody>
					</table>
				</div>
				<div>
					<form onSubmit={this.handleLogin}>
						<label className="gameTitle-label" htmlFor="gameTitle">Title: </label>
						<input className="gameTitle-input" name="gameTitle"/>                        

						<label className="gameOwner-label" htmlFor="gameOwner">Owner: </label>
						<input className="gameOwner-input" name="gameOwner"/>                        

						<label className="gameNumParticipants-label" htmlFor="gameNumParticipants">Num Participant: </label>
						<input className="gameNumParticipants-input" name="gameNumParticipants"/>                        

						<input className="create-btn btn" type="submit" value="create"/>
					</form>
					{/* {this.renderErrorMessage()} */}
				</div>			
			</div>
		)
	}
}