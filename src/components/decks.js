import React from 'react';

import Heap from '../models/Heap';
import Deck from '../models/Deck';
import Validations from '../helpers/Validations';
import Utils from '../helpers/Utils';
import Participants from '../models/Participants';

export default class Decks extends React.Component {
	constructor(args) {
		super(...args);
		this.state = {
			deckClass: ''

		};
		this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler() {
		const { game, deckClicked } = this.props;
		var currentPlayer = new Participants().getCurrentPlayer(game.players);
		if (!game.navigationMode && !new Validations().hasMoves(currentPlayer, game) && !game.isTakiState) {
			this.state.deckClass = 'start';
			this.setState(this.state);
			deckClicked();
			var that = this;
			setTimeout(function() {
				that.state.deckClass = '';
				that.setState(that.state);
			}, 3000);
		}
	};

	render() {
		const { game, heapClass } = this.props;
		let topCard = new Heap().top(game.heap);

		if (Array.isArray(topCard)) {
			topCard = topCard[0];
		}

		let filePath;
		if (topCard) {
			filePath = new Utils().getCardFilepath(topCard);
		}
		else {
			filePath = 'images/cards/cover.png';
		}

		const currentPlayer = new Participants().getCurrentPlayer(game.players);
		const myUsername = localStorage.getItem('myusername');
		const isMe = myUsername === currentPlayer.name;

		if (!this.state.deckClass.includes('start')) {
			if (game.navigationMode || (currentPlayer && !currentPlayer.isHuman)) {
				this.state.deckClass = 'disabled';
			}
			if (!game.navigationMode && !new Validations().hasMoves(currentPlayer, game) && isMe && !game.isTakiState) {
				this.state.deckClass = 'pulse';
			}
		}

		return (<div className="board-flex-container">
			<div id="deck-container" onClick={this.clickHandler}>
				<div id="deck" className={this.state.deckClass}>
					<img src="images/cards/cover.png"/>
				</div>
				<div id="deck-count">{new Deck().getSize(game.deck)}</div>
			</div>
			<div id="heap-container">
				<div id="heap" className={heapClass}>
					<img id="heap-img" src={filePath} className={new Heap().top(game.heap).color}/>
				</div>
				<div id="heap-count">{new Heap().getSize(game.heap)}</div>
			</div>
		</div>);
	}
}