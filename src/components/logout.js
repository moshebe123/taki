import React from 'react';

export default class Logout extends React.Component {

	handleLogout(e) {

		fetch('/users/logout', { method: 'GET', credentials: 'include' }).then(response => {
			if (!response.ok) {
				return false;
			}
			return response;
		}).then(content => {
			if (content) {
				this.props.onLoggedOut();
			}
		});
	}

	render() {
		return (<div>
				<button onClick={this.handleLogout.bind(this)} className="submit-btn logout">Logout</button>
			</div>);
	}
}