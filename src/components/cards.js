import React from 'react';
import ReactDOM from 'react-dom';
import CardComponent from './card';

export default function Cards(props) {
	const { player, cardClicked, onColorChoosed, game } = props;
	const infoText = `player name: ${player.name} | number of cards: ${player.cards.length}`;
	return (<div>
		<div className="player-info">
			<span>{infoText}</span>
		</div>
		{player.cards.map(
			(card, i) => <CardComponent key={i} game={game} player={player} card={card} cardClicked={cardClicked}
																	onColorChoosed={onColorChoosed}/>)}</div>);

}