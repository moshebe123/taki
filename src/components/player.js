import React from 'react';

import CardsComponent from './cards';
import PlayerInfoComponent from './playerInfo';

export default class Player extends React.Component {
	render() {
		const { game, player, cards, cardClicked, onColorChoosed } = this.props;
		return (<div>
			{/*<PlayerInfoComponent player={player}/>*/}
			<CardsComponent game={game} cards={cards} player={player} cardClicked={cardClicked}
											onColorChoosed={onColorChoosed}/>
		</div>);
	}
}