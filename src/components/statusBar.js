import React from 'react';
import Game from '../models/Game';
import Heap from '../models/Heap';
import Participants from '../models/Participants';

export default class StatusBar extends React.Component {
	constructor(args) {
		super(...args);
	}

	componentDidMount() {
		this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	millisToMinutesAndSeconds(millis) {
		var minutes = Math.floor(millis / 60000);
		var seconds = ((millis % 60000) / 1000).toFixed(0);
		return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
	}

	getTime() {
		if (this.state && this.state.time) {
			return this.state.time;
		}
		else {
			return new Date();
		}
	}

	render() {
		const { game } = this.props;

		if (!game) {
			return (<div></div>);
		}

		const time = this.getTime();

		if (new Game().isGameOver(game) || game.finished) {
			return null;
		}
		else if (!game.finished) {
			const currentPlayer = new Participants().getCurrentPlayer(game.players);
			var currentColor = new Heap().top(game.heap).color || game.heap.currentColor;

			if (currentColor && currentColor.toLowerCase) {
				currentColor = currentColor.toLowerCase();
			}
			if (currentColor && currentColor.color) {
				currentColor = currentColor.color.toLowerCase();
			}

			const startTime = new Date(game.statistics.startTime).getTime();
			const duration = this.millisToMinutesAndSeconds((time - startTime));

			let avgTurnDuration = this.millisToMinutesAndSeconds(currentPlayer.statistics.averageTurnDuration);
			if (!currentPlayer.isHuman) {
				avgTurnDuration = '-';
			}

			const message = (game.message ? `Message: ${game.message}` : '');

			return (<div id="game-info">
				<span id="current-player">Current player: {currentPlayer.name}</span>
				<span id="duration">Duration: {duration}</span>
				<span id="avg-turn-duration">Average turn duration: {avgTurnDuration}</span>
				<span id="single-card-counter">Single card: {currentPlayer.singleCardCounter}</span>
				<span id="turn-counter">Turn: {game.statistics.turns}</span>
				<span id="color">Color: {currentColor}</span>
				<span id="message">{message}</span>
			</div>);
		}
	}
}