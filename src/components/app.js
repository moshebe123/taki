import React from 'react';
import Login from './login';
import GamesDashboard from './dashboard/gamesDashboard';
import GameRoom from './gameRoom';
import Logout from './logout';

export default class App extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLogged: false, joinedGame: null
		};
	}

	handleLogin(isLogged) {
		const state = this.state;
		state.isLogged = isLogged;
		this.setState(state);
	}

	handleLoggedOut() {
		const state = this.state;
		state.isLogged = false;
		this.setState(state);
	}

	handleJoinedGame(title) {
		console.log('handleJoinedGame:', title);
		const state = this.state;
		state.joinedGame = title;
		this.setState(state);
	}

	changeState(newState) {
		this.setState(Object.assign(this.state, newState));
	}

	renderLoggedIn() {
		console.log(this.state);
		return (

			<div>
				{(this.state.joinedGame == null) ? <Logout onLoggedOut={this.handleLoggedOut.bind(this)}/> : ''}

				{(this.state.joinedGame != null) ?
					<GameRoom title={this.state.joinedGame} onLoggedOut={this.handleLoggedOut.bind(this)}
										changeState={this.changeState.bind(this)}/> :
					<GamesDashboard onJoined={this.handleJoinedGame.bind(this)}/>}

			</div>);
	}

	render() {
		const { isLogged, errorMessage } = this.state;

		return (<div>
			{isLogged ? this.renderLoggedIn() : <Login onLogin={this.handleLogin.bind(this)}/>}
		</div>);
	}
}