import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import GameRoom from './components/gameRoom';

/* Directly adding react element */                    
ReactDOM.render(    
    <App />,
    document.getElementById("root")
);