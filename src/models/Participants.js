var Player = require('./Player');

var Participants = function() {
	this.players = [];
	this.current = 0;
	this.offset = 1;
};

Participants.prototype.get = function(participants, i) {
	return participants.players[i];
};

Participants.prototype.getCount = function() {
	return this.players.length;
};

// Participants.prototype.addPlayer = function(player) {
// 	player.id = this.players.length;
// 	this.players.push(player);
// };

Participants.prototype.getCurrentPlayer = function(participants) {
	this.adjustCurrent(participants);
	return participants.players[participants.current];
};

Participants.prototype.adjustCurrent = function(participants) {	
	if (participants.current >= participants.players.length) {
		participants.current = 0;
	}
	else if (participants.current < 0) {
		participants.current = participants.players.length - 1;
	}
};

Participants.prototype.moveNext = function(players) {	
	new Player().updateStatistics(this.getCurrentPlayer(players));
	players.current += this.offset;
	var player = this.getCurrentPlayer(players);
	player.statistics.currentTurnStartedAt = new Date();
	return player;
};

Participants.prototype.changeDirection = function() {
	this.offset *= -1;
	this.next += this.offset;
};


// TODO: remove
// Participants.prototype.generate = function(numHumans, numPCs) {
// 	var i;

// 	for (i = 0; i < numHumans; i++) {
// 		var p = new Player('Human#' + i);
// 		p.isHuman = true;
// 		this.addPlayer(p);
// 	}

// 	for (i = 0; i < numPCs; i++) {
// 		this.addPlayer(new Player('PC#' + i));
// 	}

// };

Participants.prototype.addPlayer = function(participants, name, id, isHuman) {
	var player = new Player(name);
	player.id = id;
	player.isHuman = isHuman;

	participants.players.push(player);
}

module.exports = Participants;