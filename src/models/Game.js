var Deck = require('./Deck');
var Heap = require('./Heap');
var Participants = require('./Participants');
var Utils = require('../helpers/Utils');

var Game = function(id) {
	this._id = id;
	this.currentColor = null;
	this.deck = new Deck();
	this.heap = new Heap();
	this.isTakiState = false;
	this.plus2counter = 0;
	this.statistics = {
		startTime: new Date(), turns: 0
	};
	this.players = new Participants();
	this.winners = [];
	this.quiters = [];
	this.finished = false;
	this.message = '';
	this.steps = []; // for auditing and navigation between steps
	this.turnPosition = 0;
	this.navigationMode = false;
};

Game.prototype.removePlayer = function(game, player) {
	var index = game.players.players.indexOf(player);
	game.players.players.splice(index, 1);
};

Game.prototype.isCurrentHuman = function() {
	return new Participants().getCurrentPlayer(this.players).isHuman;
};

Game.prototype.start = function() {
	this.finished = false;

	this.updateMessage('Starting !');
	this.deck.shuffle();
	this.deck.moveUntilValidTop(this.heap);
	var deck = this.deck;

	this.players.players.forEach(function(player) {
		player.cards = deck.pullCards(8);
	});

	this.addStep(this, 'start', null, new Participants().getCurrentPlayer(this.players));

	if (!this.isCurrentHuman()) {
		this.makeTurn(this);
	}
};

Game.prototype.getWinner = function(game) {
	var winner;

	if (!new Utils().isEmpty(game.winners)) {
		winner = game.winners[0];
	}
	else {
		winner = new Participants().get(game.players, 0);
	}
	return winner;
};

Game.prototype.updateMessage = function(msg) {
	console.log('message: ' + msg);
	this.message = msg;
};

Game.prototype.isGameOver = function(game) {
	const res = game.players && game.players.players.length <= 1;
	return res;
};

Game.prototype.finishTakiTurn = function(game) {
	game.makeTurn(game, function(args) {
		var game = args.game;

		game.isTakiState = false;
		game.heap.currentColor = null;

		return { updateTurnInfo: true, moveNextPlayer: true };
	}, { game: game });
};

Game.prototype.updateStatistics = function(game) {
	var stats = game.statistics;
	stats.turns++;
};

Game.prototype.turn = function(callback, args) {

	// console.log('turn. currentPlayer: ', this.players.getCurrentPlayer().name);

	var result = {};

	if (callback) {
		result = callback(args);
	}

	if (result.updateTurnInfo) {
		this.updateStatistics(this);
	}
	if (result.moveNextPlayer) {
		this.updateMessage('Switch turn');
		new Participants().moveNext(this.players);
	}
	if (result.step) {
		this.addStep(this, result.step.card, result.step.color, result.step.player);
	}

	var currentPlayer = this.players.getCurrentPlayer();

	if (Utils.isEmpty(currentPlayer.cards)) {
		this.removePlayer(game, currentPlayer);
		this.winners.push(currentPlayer);

	}
	else if (currentPlayer.cards.length === 1) {
		currentPlayer.singleCardCounter++;
	}

	if (this.players.players.length === 1) {
		var currentPlayer = this.players.getCurrentPlayer();
		this.removePlayer(game, currentPlayer);
		this.winners.push(currentPlayer);
	}

	if (this.isGameOver(this)) {
		if (this.winners.indexOf(currentPlayer) === -1) {
			this.winners.push(currentPlayer);
		}

		this.handleEndGame();
	}
	else {
		if (!this.isCurrentHuman()) {
			console.log('Invokig pc turn...');
			raisePcEvent();

		}
	}
};

Game.prototype.makeTurn = function(game, callback, args) {

	var result = {};

	if (callback) {
		result = callback(args);
	}

	if (result.updateTurnInfo) {
		new Game().updateStatistics(game);
	}
	if (result.moveNextPlayer) {
		game.updateMessage('Switch turn');
		new Participants().moveNext(game.players);
	}
	if (result.step) {
		new Game().addStep(game, result.step.card, result.step.color, result.step.player);
	}

	var currentPlayer = new Participants().getCurrentPlayer(game.players);

	if (new Utils().isEmpty(currentPlayer.cards)) {
		new Game().removePlayer(game, currentPlayer);
		game.winners.push(currentPlayer);

	}
	else if (currentPlayer.cards.length === 1) {
		currentPlayer.singleCardCounter++;
	}

	if (game.players.players.length === 1) {
		var currentPlayer = new Participants().getCurrentPlayer(game.players);
		new Game().removePlayer(game, currentPlayer);
		game.winners.push(currentPlayer);
	}

	if (new Game().isGameOver(game)) {
		if (game.winners.indexOf(currentPlayer) === -1) {
			game.winners.push(currentPlayer);
		}

		new Game().handleEndGame(game);
	}
	else {
		if (!game.isCurrentHuman()) {
			console.log('Invokig pc turn...');
			// raisePcEvent();

		}
	}
};

Game.prototype.handleEndGame = function(game) {
	this.updateMessage('Game Finished!!!!!');
	var winner = this.getWinner(game);

	game.quiters.forEach(function(player) {
		game.winners.push(player);
	});

	game.finished = true;
	if (!game.statistics.endGame) {
		game.statistics['endGame'] = new Date();
	}

	var winnerName = 'No one';
	if (winner && winner.name) {
		winnerName = winner.name;
	}
	this.updateMessage('Game Finished!!!!! The Winner is ' + winnerName);
};

Game.prototype.addStep = function(game, card, color, player) {
	game.steps.push({
		card,
		color,
		player,
		players: JSON.parse(JSON.stringify(game.players)),
		deck: JSON.parse(JSON.stringify(game.deck)),
		heap: JSON.parse(JSON.stringify(game.heap)),
		winners: JSON.parse(JSON.stringify(game.winners)),
		quiters: JSON.parse(JSON.stringify(game.quiters))
	});
	game.turnPosition++;
};

function gup(name, url) {
	if (!url) {
		url = location.href;
	}
	name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
	var regexS = '[\\?&]' + name + '=([^&#]*)';
	var regex = new RegExp(regexS);
	var results = regex.exec(url);
	return results == null ? null : results[1];
}

function raisePcEvent() {
	console.log('raisePcEvent');
	var event = new CustomEvent('pc-event', {
		detail: {}
	});
	window.dispatchEvent(event);
}

// function startGame() {
// 	var url = window.location.href;
// 	var numHumanPlayers = gup('num_human_players', url) || 1;
// 	var numPcPlayers = gup('num_pc_players', url) || 1;

// 	// var participants = new Participants();
// 	// participants.generate(numHumanPlayers, numPcPlayers);

// 	var game = new Game('<game-id>');
// 	window.game = game;
// 	window.game.finished = false;

// 	game.start();
// }

module.exports = Game;