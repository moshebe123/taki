var Card = require('./Card');
var Heap = require('./Heap');

var Deck = function() {
	this.cards = this.generateCards();
};

Deck.prototype.generateCards = function() {
	var numbers = [
		{
			number: 1, amount: 2
		}, {
			number: 3, amount: 2
		}, {
			number: 4, amount: 2
		}, {
			number: 5, amount: 2
		}, {
			number: 6, amount: 2
		}, {
			number: 7, amount: 2
		}, {
			number: 8, amount: 2
		}, {
			number: 9, amount: 2
		}
	];
	var actionsWithColor = [
		{
			operation: 'taki', amount: 2
		}, {
			operation: 'stop', amount: 2
		}, {
			operation: 'plus', amount: 2
		}, {
			operation: 'plus2', amount: 2
		}, {
			operation: 'changeDirection', amount: 2
		}
	];

	var actionsWithoutColor = [
		{ operation: 'changeColor', amount: 4 }, { operation: 'superTaki', amount: 2 }
	];

	var colorEnum = {
		GREEN: { value: 'green', cssColor: '#88c750' },
		RED: { value: 'red', cssColor: '#ff8269' },
		YELLOW: { value: 'yellow', cssColor: '#f4c764' },
		BLUE: { value: 'blue', cssColor: '#4e9cff' }
	};

	var cardWithColor = numbers.concat(actionsWithColor);
	var cardsWithoutColor = actionsWithoutColor;
	var res = [];
	cardWithColor.forEach(function(card) {
		Object.keys(colorEnum).forEach(function(color) {
			for (var i = 0; i < card.amount; i++) {
				res.push(new Card(card.number, card.operation, color, ''));
			}
		});
	});
	cardsWithoutColor.forEach(function(card) {
		for (var i = 0; i < card.amount; i++) {
			res.push(new Card(card.number, card.operation, null, ''));
		}
	});
	return res;
};

Deck.prototype.pullCards = function(num) {
	var res = [];
	for (var i = 0; i < num; i++) {
		res.push(this.pull());
	}

	return res;
};

Deck.prototype.pull = function() {
	var res = this.cards.shift();
	if (this.cards.length === 0) {
		this.cards = game.heap.pullAllWithoutLast();
		this.shuffle();
	}
	return res;
};

Deck.prototype.shuffle = function() {
	var j, x, i;
	for (i = this.cards.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = this.cards[i];
		this.cards[i] = this.cards[j];
		this.cards[j] = x;
	}
};

Deck.prototype.moveUntilValidTop = function(heap) {
	var top;
	do {
		top = this.pull();
		heap.put(top);
	}
	while (top.number == null);

	return top;
};

Deck.prototype.getSize = function(deck) {
	return deck.cards.length;
};

module.exports = Deck;