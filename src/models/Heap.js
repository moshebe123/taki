var Utils = require('../helpers/Utils');

var Heap = function() {
	this.removeAll();
	this.currentColor = null;
};

Heap.prototype.put = function(card) {	
	if (card) {
		this.cards.push(card);
		if (card.color){
			this.currentColor = card.color;
		}
	}
	
};

Heap.prototype.setRandomColor = function() {
	this.currentColor = new Utils().getRandomColor();
};

Heap.prototype.getSize = function(heap) {
	return heap.cards.length;
};

Heap.prototype.getCards = function() {
	return this.cards;
};

Heap.prototype.removeAll = function() {
	this.cards = [];
};

Heap.prototype.top = function(heap) {
	if (heap && heap.cards.length > 0) {
		return heap.cards[heap.cards.length - 1];
	}
	return null;
};

Heap.prototype.pullAllWithoutLast = function() {
	var res = this.cards.slice(0, -1);
	this.cards = this.cards.slice(-1);
	return res;
};

module.exports = Heap;