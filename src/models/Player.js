var Utils = require('../helpers/Utils');

var Player = function(name) {
	this.name = name;
	this.isHuman = false;
	this.cards = [];
	this.id = -1;
	this.singleCardCounter = 0;
	this.statistics = {
		turns: 0, totalTurnsDuration: 0, averageTurnDuration: 0, currentTurnStartedAt: new Date()
	};
};

Player.prototype.removeCard = function(card) {
	const index = this.cards.findIndex(c => c.isEqual(card));
	this.cards.splice(index,1);
};

Player.prototype.updateStatistics = function(player) {
	var stats = player.statistics;
	stats.turns++;
	stats.totalTurnsDuration += (new Date() - stats.currentTurnStartedAt);
	stats.averageTurnDuration = stats.totalTurnsDuration / stats.turns;
};

Player.prototype.pullCard = function(player, game) {
	var amount = (game.plus2counter) ? game.plus2counter : 1;
	// console.log(`going to pull ${amount} cards. Player: ${player.name} currently have ${player.cards.length}`);
	for (var i = 0; i < amount; i++) {
		var pulledCards = game.deck.pull(); // TODO: maybe need to be changed to pass this externally
		player.cards.push(pulledCards);
	}
	if (game.plus2counter) {
		// console.log(`just pull ${amount} cards. Player: ${player.name} currently have ${player.cards.length}. plus2counter: ${game.plus2counter}`);
		game.plus2counter = 0;
	}
	return true;
};

Player.prototype.putCard = function(heap, card) {	
	heap.put(card);
	var index = this.cards.indexOf(card);
	this.removeCard(card);
};

Player.prototype.appendCardToResult = function(index, res) {
	var card = this.cards[index];
	// res.push({ index, card });
	res.push(card);
	//this.removeCard(index);
	return res;
};
Player.prototype.getCardsByColor = function(color) {
	var res = [];
	for (var i = this.cards.length - 1; i >= 0; i--) {
		if (isSameColor(this.cards[i], color)) {
			res = this.appendCardToResult(i, res);
		}
	}

	return res;
};

function isSameColor(card, color) {
	return card.color && color && card.color.toLowerCase && color.toLowerCase && card.color.toLowerCase() === color.toLowerCase();

}

Player.prototype.getCardsByNumber = function(number) {
	var res = [];
	for (var i = this.cards.length - 1; i >= 0; i--) {
		if (number && this.cards[i].number === number) {
			res = this.appendCardToResult(i, res);
		}
	}

	return res;
};

Player.prototype.getCardsByOperation = function(operation, color) {
	var res = [];
	for (var i = this.cards.length - 1; i >= 0; i--) {
		if (this.cards[i].operation === operation) {
			if (!color || this.cards[i].color == color) {
				res = this.appendCardToResult(i, res);
			}
		}
	}

	return res;
};

// result: {action: <pull|put|changeColor|superTaki|plus>, cards: [{index, card}]}
Player.prototype.getPcMove = function(game) {
	var cards = [];

	var currentCard = new Heap().top(game.heap);
	var currentColor = game.heap.currentColor;

	if (currentCard.operation === 'plus2') {
		cards = this.getCardsByOperation('plus2');
		if (!Utils.isEmpty(cards)) {
			return { action: 'plus2', cards: Utils.takeOne(cards) };
		}
		else {
			return { action: 'pull' };
		}
	}

	cards = this.getCardsByOperation('plus2', currentColor);
	if (!Utils.isEmpty(cards)) {
		return { action: 'plus2', cards: Utils.takeOne(cards) };
	}

	cards = this.getCardsByOperation('changeColor');
	if (!Utils.isEmpty(cards)) {
		return { action: 'changeColor', cards: Utils.takeOne(cards) };
	}

	cards = this.getCardsByOperation('stop', currentColor);
	if (!Utils.isEmpty(cards)) {
		return { action: 'stop', cards: Utils.takeOne(cards) };
	}

	cards = this.getCardsByOperation('plus', currentColor);
	if (!Utils.isEmpty(cards)) {
		return { action: 'plus', cards: Utils.takeOne(cards) };
	}

	cards = this.getCardsByOperation('superTaki');
	if (!Utils.isEmpty(cards)) {
		cards = Utils.takeOne(cards);
		cards.push(this.getCardsByColor(currentColor));

		return { action: 'superTaki', cards: cards };
	}

	cards = this.getCardsByOperation('taki', currentColor);
	if (!Utils.isEmpty(cards)) {
		cards = Utils.takeOne(cards);
		cards.push(this.getCardsByColor(currentColor));

		return { action: 'taki', cards: cards };
	}

	cards = this.getCardsByColor(currentColor);
	if (!Utils.isEmpty(cards)) {
		return { action: 'put', cards: Utils.takeOne(cards) };
	}

	cards = this.getCardsByNumber(currentCard.number);
	if (!Utils.isEmpty(cards)) {
		return { action: 'put', cards: Utils.takeOne(cards) };
	}

	return { action: 'pull' };
};

module.exports = Player;