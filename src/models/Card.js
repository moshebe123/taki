var Card = function(number, operation, color, img) {
	this.number = number;
	this.operation = operation;
	this.color = color;
	this.img = img;
};

Card.prototype.getValue = function() {
	if (this.operation) {
		return this.operation;
	}
	else {
		return this.number;
	}
};

Card.prototype.getColor = function() {
	return this.color || 'nocolor';
};

Card.prototype.stringify = function() {
	return this.getValue() + ' ' + this.getColor();
};

Card.prototype.isSameColor = function(color) {
	return this.color && color && this.color.toLowerCase() === color.toLowerCase();
};

Card.prototype.getFilename = function() {
	var filename = this.getValue() + '_' + this.getColor().toLowerCase() + '.png';
	return filename;
};

Card.prototype.getFilepath = function(filename) {
	return `images/cards/${(filename || this.getFilename())}`;
};

Card.prototype.isEqual = function(card) {
	if (!card) {
		debugger;
	}
	if (this.color !== card.color) {
		return false;
	}
	if (this.operation !== card.operation) {
		return false;
	}
	if (this.number !== card.number) {
		return false;
	}

	return true;

};

module.exports = Card;