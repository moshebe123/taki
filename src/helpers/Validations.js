var ViewUtils = require('./ViewUtils');
var Heap = require('../models/Heap');

var Validations = function() {
};

Validations.prototype.isValidCard = function(card, game) {
	var topCard = new Heap().top(game.heap);
	var currentColor = game.heap.currentColor;
	if (currentColor && currentColor.color) {
		currentColor = currentColor.color;
	}

	if (game.plus2counter && card.operation === 'plus2') {
		return true;
	}
	else if (game.plus2counter === 0) {
		if (card.color && game.isTakiState) {
			return new ViewUtils().isSameColor(card.color, currentColor);
		}

		if (card.color && new ViewUtils().isSameColor(card.color, currentColor)) {
			return true;
		}

		if (card.number && card.number === topCard.number) {
			return true;
		}

		if (card.operation && topCard.operation && card.operation === topCard.operation) {
			return true;
		}

		if (card.operation === 'changeColor' || card.operation === 'superTaki') {
			return true;
		}
	}
	return false;
};

Validations.prototype.hasMoves = function(player, game) {
	var res = false;
	if (player && player.cards) {
		player.cards.forEach(function(card) {
			if (new Validations().isValidCard(card, game)) {
				res = true;
			}
		});
	}
	return res;
};

module.exports = Validations;