var ViewUtils = require('./ViewUtils');

var Utils = function() {
};

Utils.prototype.gup = function(name, url){
	if (!url) {
		url = location.href;
	}
	name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
	var regexS = '[\\?&]' + name + '=([^&#]*)';
	var regex = new RegExp(regexS);
	var results = regex.exec(url);
	return results == null ? null : results[1];
};

Utils.prototype.isEmpty = function(arr) {
	return !arr || arr.length == 0;
};

Utils.prototype.takeOne = function(arr) {
	return arr.slice(-1);
}

Utils.prototype.getRandomColor = function() {
	var colors = ['red', 'green', 'blue', 'yellow'];
	return colors[Math.floor(Math.random() * colors.length)];
};

Utils.prototype.millisToMinutesAndSeconds = function(millis) {
	var minutes = Math.floor(millis / 60000);
	var seconds = ((millis % 60000) / 1000).toFixed(0);
	return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
};

Utils.prototype.getCardFilepath = function(card, filename) {
	if (card) {
		filename = new ViewUtils().getFilename(card);
	}
	return `images/cards/${(filename)}`;
}

module.exports = Utils;