var Participants = require('../models/Participants');
var Game = require('../models/Game');


var GameCreator = function(){
};

function restartGame(title) {
    // TODO: check number of human players & pc players
    const { numParticipants } = gamesList[title];

    gamesList[title].game = new GameCreator().createGame(numParticipants, 0, title);
    gamesList[title].started = true;
}
GameCreator.prototype.createGame = function(gameId){
	// var participants = new Participants();
	// participants.generate(numHumans, numComputers);
	
	const game = new Game(gameId);
		
	// game.deck.shuffle();
	// game.deck.moveUntilValidTop(game.heap);
	// var deck = game.deck;
	
	// game.players.players.forEach(function(player) {
	// 	player.cards = deck.pullCards(8);
	// });
	
	// game.start();
	
	return game;
};

GameCreator.prototype.createStandardGame = function(){
	return this.createGame(1, 1, '<game-id>');
};

module.exports = GameCreator;