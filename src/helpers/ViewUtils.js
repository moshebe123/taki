var ViewUtils = function(){
};

ViewUtils.prototype.getFilename = function(card){
	if (!card) {
		return;
	}
	var value = (card.operation) ? card.operation : card.number;
	var color = card.color || 'nocolor';
	if (!value) {
		debugger;
	}
	var filename = value + '_' + color.toLowerCase() + '.png';
	return filename;
};

ViewUtils.prototype.getFilepath = function(filename, card) {
	if (card) {
		filename = this.getFilename(card);
	}
	return `images/cards/${(filename)}`;
};

ViewUtils.prototype.isSameColor = function(currentColor, cardColor) {
	return cardColor && currentColor && cardColor.toLowerCase && currentColor.toLowerCase && cardColor.toLowerCase() === currentColor.toLowerCase();


};

module.exports = ViewUtils;