const path = require('path');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const userManagement = require('./userManagement');
const gameManagement = require('./gameManagement');
const auth = require('./auth');

var app = express();

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3001;

app.use(session({ secret: 'keyboard cat', cookie: {maxAge:269999999999}}));
app.use(bodyParser.text());

app.use(express.static(publicPath));

app.use('/users', userManagement);
app.use('/games', gameManagement);

app.listen(port, () => {
	console.log(`Listening on port ${port}!`);
});