const express = require('express');
const auth = require('./auth');

const userManagement = express.Router();

userManagement.get('/clear', (req, res) => {	
	auth.clear();
	res.sendStatus(200);
});

userManagement.get('/', auth.userAuthentication, (req, res) => {
	const userName = auth.getUserInfo(req.session.id).name;
	res.json({name:userName});
});

// TODO: userManagement.get('/allUsers', auth.userAuthentication, (req, res) => {
	userManagement.get('/allUsers', (req, res) => {	
		const userList = auth.getUsersList();
		const result = Object.keys(userList).map(i => ({
		name: userList[i],
		id: i
	}))
	res.json(result);
});

userManagement.post('/addUser', auth.addUserToAuthList, (req, res) => {		
	res.sendStatus(200);	
});

userManagement.get('/logout', [
	(req, res, next) => {	
		const userinfo = auth.getUserInfo(req.session.id);	
		// chatManagement.appendUserLogoutMessage(userinfo);
		next();
	}, 
	auth.removeUserFromAuthList, 
	(req, res) => {
		res.sendStatus(200);		
	}]
);


module.exports = userManagement;