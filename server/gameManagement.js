const express = require('express');
const auth = require('./auth');
const games = require('./games');
const Participants = require('../src/models/Participants');
const Game = require('../src/models/Game');
const Heap = require('../src/models/Heap');
const Player = require('../src/models/Player');
const Validations = require('../src/helpers/Validations');

const gameManagement = express.Router();

gameManagement.get('/', auth.userAuthentication, (req, res) => {
	const title = games.getGameInfo(req.body.title).title;
	res.json({ title: title });
});

gameManagement.get('/start/:title', auth.userAuthentication, (req, res) => {
	const title = req.params.title;
	const game = games.getOrCreateIfNotExists(title);
	res.sendStatus(200);
});

gameManagement.get('/restart/:title', auth.userAuthentication, (req, res) => {
	const title = req.params.title;
	games.restartGame(title);
	res.sendStatus(200);
});

gameManagement.get('/state/:title', auth.userAuthentication, (req, res) => {
	const title = req.params.title;
	const game = games.getGame(title);
	res.json(game);
});

gameManagement.get('/allGames', auth.userAuthentication, (req, res) => {
	const gamesList = games.getGames();
	const result = Object.keys(gamesList).map(i => ({
		game: gamesList[i], id: i
	}));
	res.json(result);
});

gameManagement.post('/create', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);

	if (games.isGameExists(payload.title)) {
		res.status(403).send('Game already exists');
	}
	else {
		const userInfo = auth.getUserInfo(req.session.id);
		games.createGame(payload.title, userInfo.name, payload.numParticipants);
		res.sendStatus(200);
	}
});

gameManagement.post('/remove', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	console.log(payload);
	if (!games.isGameExists(payload.gameId)) {
		res.status(403).send('Game does not exists');
	}
	else {
		games.removeFromGamesList(payload.gameId);
		res.sendStatus(200);
	}
});
gameManagement.post('/init', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	console.log(payload);
	if (!games.isGameExists(payload.gameId)) {
		res.status(403).send('Game does not exists');
	}
	else {
		games.initGame(payload.gameId);
		res.sendStatus(200);
	}
});
// TODO: change to post
// gameManagement.get('/join2/:title/:username', auth.userAuthentication, (req, res) => {					
// 	const title = req.params.title;
// 	const sessionId = req.session.id;
// 	const username = req.params.username;

// 	games.joinGame(title, username, sessionId, true);

// 	res.sendStatus(200);	
// });

gameManagement.post('/join', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);

	const { title, name, isHuman = 1 } = payload;
	const sessionId = req.session.id;

	games.joinGame(title, name, sessionId);

	res.sendStatus(200);
});

gameManagement.post('/putCard', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	const game = games.getGame(payload.game);

	if (!new Game().isGameOver(game.game) && !game.game.finished) {
		const card = payload.card;
		const currentPlayer = new Participants().getCurrentPlayer(game.game.players);

		var args = {};
		args.card = card;
		args.player = currentPlayer;
		args.game = game.game;
		new Game().makeTurn(game.game, function(args) {
			var updateTurnInfo = true;
			var moveNextPlayer = true;

			var player = args.player;
			var card = args.card;
			var game = args.game;

			var currentColor = new Heap().top(game.heap) || game.heap.currentColor;

			player.putCard(game.heap, card);
			heapClass = 'start';
			if (card.operation === 'changeDirection') {
				game.players.offset *= -1;
				game.players.next += game.players.offset;
			}
			if (card.operation === 'plus2') {
				game.plus2counter += 2;
				// console.log(`Player: ${player.name} just put plus2. plus2counter: ${game.plus2counter}`);
			}
			else if (card.operation === 'superTaki') {
				game.heap.currentColor = currentColor.color;
				console.log(`super taki current color ${JSON.stringify(currentColor)}`);
				// card.color = currentColor.color;
				if (new Validations().hasMoves(player, game)) {
					game.isTakiState = true;
				}
			}
			if (card.operation === 'taki') {
				if (new Validations().hasMoves(player, game)) {
					game.isTakiState = true;
					game.heap.currentColor = card.color;
				}
			}
			else if (card.operation === 'stop' && !game.isTakiState) {
				game.players.moveNext();
			}
			else if (card.operation === 'plus') {
				moveNextPlayer = false;
			}
			if (game.isTakiState) {
				updateTurnInfo = false;
				moveNextPlayer = false;
			}
			var step = { card, color: card.color, player };
			return { updateTurnInfo, moveNextPlayer, step };
		}, args);

	}
	res.sendStatus(200);
});

gameManagement.post('/pullDeck', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	const game = games.getGame(payload.game);
	const currentPlayer = new Participants().getCurrentPlayer(game.game.players);

	// TOOD: add this check
	// if (Validations.hasMoves(currentPlayer, game)) {
	// 	game.updateMessage('Card cannot be pulled when there are valid cards to put');
	// 	return;
	// }

	var args = {};
	args.player = currentPlayer;
	args.game = game.game;

	new Game().makeTurn(game.game, function(args) {
		new Player().pullCard(args.player, args.game);
		return { updateTurnInfo: true, moveNextPlayer: true };
	}, args);

	res.sendStatus(200);
});

gameManagement.post('/quit', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	const game = games.getGame(payload.game);
	const currentPlayer = new Participants().getCurrentPlayer(game.game.players);

	game.game.quiters.push(currentPlayer);
	new Game().removePlayer(game.game, currentPlayer);
	new Game().makeTurn(game.game);

	res.sendStatus(200);
});

gameManagement.post('/commitTaki', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	const game = games.getGame(payload.game);

	new Game().finishTakiTurn(game.game);

	res.sendStatus(200);
});

gameManagement.post('/colorChoose', auth.userAuthentication, (req, res) => {
	const payload = JSON.parse(req.body);
	const game = games.getGame(payload.game);
	const currentPlayer = new Participants().getCurrentPlayer(game.game.players);

	const args = {
		game: game.game, color: payload.color, card: payload.card, player: currentPlayer
	};

	new Game().makeTurn(game.game, function(args) {
		const { game, card, player, color } = args;

		player.putCard(game.heap, card);
		game.heap.currentColor = color.toUpperCase();

		var step = { card, color, player };
		return { updateTurnInfo: true, moveNextPlayer: true, step };
	}, args);

	res.sendStatus(200);
});

module.exports = gameManagement;