var Game = require('../src/models/Game');
var Participants = require('../src/models/Participants');
const gamesList = {};

function getGameInfo(id) {
	return { game: gamesList[id] };
}

function createGame(title, owner, numParticipants) {
	const game = {
		title: title,
		owner: owner,
		numParticipants: parseInt(numParticipants),
		numRegistered: 0,
		started: false,
		game: new Game(title)
	};

	gamesList[title] = game;
}

function startGame(title) {
	const gameInfo = gamesList[title];
	const game = gameInfo.game;

	// TODO: only for debug
	while (gameInfo.numRegistered < gameInfo.numParticipants) {
		const id = gameInfo.numRegistered;
		joinGame(title, 'DummyPlayer#' + id, id, true, false);
	}

	game.deck.shuffle();
	game.deck.moveUntilValidTop(game.heap);
	var deck = game.deck;

	game.players.players.forEach(function(player) {
		player.cards = deck.pullCards(8);
	});

	game.start();
	gameInfo.started = true;
}

function restartGame(title) {
	const players = gamesList[title].game.players;
	gamesList[title].game = new Game(title);
	gamesList[title].game.players = players;
	startGame(title);
}

function initGame(title) {
	const game = {
		title: title,
		owner: gamesList[title].owner,
		numParticipants: gamesList[title].numParticipants,
		numRegistered: 0,
		started: false,
		game: new Game(title)
	};
	gamesList[title] = game;
}

function joinGame(title, name, playerId, isHuman, autoStart = true) {
	const gameInfo = gamesList[title];

	console.log('JOIN', title, name, playerId, isHuman);
	if (!gameInfo.game.players.players.find(p => p.id === playerId)) {
		new Participants().addPlayer(gameInfo.game.players, name, playerId, isHuman);

		gameInfo.numRegistered++;
		if (gameInfo.numRegistered === gameInfo.numParticipants && autoStart) {
			startGame(title);
		}
	}
}

function isGameExists(title) {
	return (title in gamesList);
}

function getGames() {
	return gamesList;
}

function getGame(title) {
	return gamesList[title];
}

function getOrCreateIfNotExists(title) {
	if (!isGameExists(title)) {
		createGame(title, 'AAA', 2);
	}

	return getGame(title);
}

function removeFromGamesList(gameId) {
	delete gamesList[gameId];
}

module.exports = {
	getGameInfo,
	createGame,
	removeFromGamesList,
	isGameExists,
	getGames,
	getGame,
	joinGame,
	gamesList,
	getOrCreateIfNotExists,
	startGame,
	restartGame,
	initGame
};